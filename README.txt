DESCRIPTION
===========
Implements Nigeria Fidelity Bank Plc Paygate payment services
for use with Drupal Commerce.

INSTALLATION INSTRUCTIONS FOR COMMERCE FIDELITY PAYGATE
===============================================

Installing & configuring the ePayment payment method in Drupal Commerce
-----------------------------------------------------------------------
- Enable the module (Go to admin/modules and search in the
  Commerce (Payment) fieldset).
- Go to Administration > Store > Configuration > Payment Methods
- Under "Disabled payment method rules", find the
  Fidelity Paygate payment method
- Click the 'enable' link
- Once "Fidelity Paygate" appears in the "Enabled payment method rules",
  click on it's name to configure
- In the table "Actions", find "Enable payment method: Fidelity Paygate" and
  click the link
- Under "Payment settings", you can configure the module:

Configuring Fidelity Paygate
--------------------

Credits
=======
Tony Ogbonna (http://drupal.org/user/867704).
Sponsored by Icelark Projects (www.icelark.com)
