<?php

/**
 * @file
 * API and hooks documentation for the Commerce Fidelity Paygate module.
 */

/**
 * Alter payment data before it is sent to Fidelity Paygate.
 *
 * Allows modules to alter the payment data before the data is signed and sent
 * to  Fidelity Paygate.
 *
 * @param array $data
 *   The data that is to be sent to  Fidelity Paygate as an associative array.
 * @param object $order
 *   The commerce order object being processed.
 * @param array $settings
 *   The configuration settings.
 */
function hook_commerce_fidelity_paygate_data_alter(&$data, $order, $settings) {
  // Data alterations here.
}
